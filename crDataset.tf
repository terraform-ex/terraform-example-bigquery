resource "google_bigquery_dataset" "dataset" {
  dataset_id                  = "TEST1"
  friendly_name               = "test"
  description                 = "This is a test description"
  location                    = "US"
  delete_contents_on_destroy = true
  default_table_expiration_ms = 3600000
}

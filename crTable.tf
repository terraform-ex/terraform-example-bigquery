resource "google_bigquery_table" "default" {
  dataset_id = google_bigquery_dataset.dataset.dataset_id
  table_id   = "table1"

  schema = <<EOF
[
  {
    "name": "label1",
    "type": "STRING",
    "mode": "NULLABLE",
    "description": "description for - label1 -"
  },
  {
    "name": "label2",
    "type": "STRING",
    "mode": "NULLABLE",
    "description": "description for - label2 -"
  }
]
EOF

}


resource "google_bigquery_table" "default2" {
  dataset_id = google_bigquery_dataset.dataset.dataset_id
  table_id   = "table2"

  schema = <<EOF
[
  {
    "name": "label1",
    "type": "STRING",
    "mode": "NULLABLE",
    "description": "description for - label1 -"
  },
  {
    "name": "label2",
    "type": "STRING",
    "mode": "NULLABLE",
    "description": "description for - label2 -"
  }
]
EOF

}

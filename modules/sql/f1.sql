DROP TABLE IF EXISTS `burnished-case-280710.TEST1.tablex5`;

CREATE EXTERNAL TABLE burnished-case-280710.TEST1.tablex5
(
   GROUP_SIGNATURE_CODE STRING,
   DESCRIPTION  STRING,
   EXTRA_ORDER_LABEL  STRING,
   SORT_ORDER  STRING,
   IPH_CODE  STRING,
   DELETED  STRING,
   DELETION_DATE  STRING,
   RUNNING_DATE  STRING,
   system_source  STRING,
   loadin_date  STRING
);
